#!/usr/bin/env python

from __future__ import print_function
from functools import partial

import re
import argparse
import hpccm
import os
from hpccm.building_blocks import *
from hpccm.primitives import *

import itertools


def generateRecipeStarPU(baseImage):
    Stage0 = hpccm.Stage()

    Stage0 += baseimage(image=baseImage)

    # Install dependencies
    Stage0 += packages(apt=['libhwloc-dev'], yum=['hwloc-devel'], epel=True)

    Stage0 += comment('TaskBench installation StarPU')
    Stage0 += environment(variables={'DEFAULT_FEATURES': 0,
                                     'USE_STARPU': 1,
                                     'TASKBENCH_USE_HWLOC': 1})
    Stage0 += generic_build(build=['mkdir -p /opt/task-bench',
                                   'cp -R ./* /opt/task-bench',
                                   'cd /opt/task-bench',
                                   'export CXX=mpicxx',
                                   './get_deps.sh',
                                   './build_all.sh'],
                            repository='https://gitlab.com/ompcluster/task-bench.git')

    hpccm.config.set_container_format('docker')

    return Stage0


def generateRecipeCharmMPI(baseImage):
    Stage0 = hpccm.Stage()

    Stage0 += baseimage(image=baseImage)

    # Packages available with the same name for both apt and yum
    Stage0 += packages(ospackages=['autoconf', 'automake'],
                       epel=True)

    Stage0 += comment('TaskBench installation Charm++')
    Stage0 += environment(variables={'DEFAULT_FEATURES': 0,
                                     'USE_CHARM': 1,
                                     'CHARM_VERSION': 'mpi-linux-x86_64'})
    Stage0 += generic_build(build=['mkdir -p /opt/task-bench',
                                   'cp -R ./* /opt/task-bench',
                                   'cd /opt/task-bench',
                                   'export CXX=mpicxx',
                                   './get_deps.sh',
                                   './build_all.sh'],
                            repository='https://gitlab.com/ompcluster/task-bench.git')

    hpccm.config.set_container_format('docker')

    return Stage0

def generateRecipeCharmUCX(baseImage):
    Stage0 = hpccm.Stage()

    Stage0 += baseimage(image=baseImage)

    # Packages available with the same name for both apt and yum
    Stage0 += packages(ospackages=['autoconf', 'automake'],
                       epel=True)

    Stage0 += comment('TaskBench installation Charm++')
    Stage0 += environment(variables={'DEFAULT_FEATURES': 0,
                                     'USE_CHARM': 1,
                                     'CHARM_VERSION': 'ucx-linux-x86_64'})
    Stage0 += generic_build(build=['mkdir -p /opt/task-bench',
                                   'cp -R ./* /opt/task-bench',
                                   'cd /opt/task-bench',
                                   'export CXX=mpicxx',
                                   './get_deps.sh',
                                   './build_all.sh'],
                            repository='https://gitlab.com/ompcluster/task-bench.git',
                            branch='change-test')

    hpccm.config.set_container_format('docker')

    return Stage0

def generateRecipeRegent(baseImage):
    Stage0 = hpccm.Stage()

    Stage0 += baseimage(image=baseImage)

    # Packages available with the same name for both apt and yum
    Stage0 += packages(ospackages=['autoconf', 'automake'],
                       epel=True)

    Stage0 += python(python2=False, python3=True)

    Stage0 += comment('TaskBench installation Regent')
    Stage0 += environment(variables={'DEFAULT_FEATURES': 0,
                                     'USE_REGENT': 1})
    Stage0 += generic_build(build=['mkdir -p /opt/task-bench',
                                   'cp -R ./* /opt/task-bench',
                                   'cd /opt/task-bench',
                                   'export CXX=mpicxx',
                                   './get_deps.sh',
                                   './build_all.sh'],
                            repository='https://gitlab.com/ompcluster/task-bench.git')

    hpccm.config.set_container_format('docker')

    return Stage0


def generateRecipeChapel(baseImage):
    Stage0 = hpccm.Stage()

    Stage0 += baseimage(image=baseImage)

    Stage0 += python()

    Stage0 += comment('TaskBench installation Chapel')
    Stage0 += environment(variables={'DEFAULT_FEATURES': 0,
                                     'USE_CHAPEL': 1,
                                     'USE_GASNET': 1,
                                     'CONDUIT': 'ibv',
                                     'CHPL_LAUNCHER': 'gasnetrun_ibv'})
    Stage0 += generic_build(build=['mkdir -p /opt/task-bench',
                                   'cp -R ./* /opt/task-bench',
                                   'cd /opt/task-bench',
                                   'export CXX=mpicxx',
                                   './get_deps.sh',
                                   './build_all.sh'],
                            repository='https://gitlab.com/ompcluster/task-bench.git')

    hpccm.config.set_container_format('docker')

    return Stage0


def generateRecipeLegion(baseImage):
    Stage0 = hpccm.Stage()

    Stage0 += baseimage(image=baseImage)

    Stage0 += comment('TaskBench installation Legion')
    Stage0 += environment(variables={'DEFAULT_FEATURES': 0,
                                     'USE_LEGION': 1})
    Stage0 += generic_build(build=['mkdir -p /opt/task-bench',
                                   'cp -R ./* /opt/task-bench',
                                   'cd /opt/task-bench',
                                   'export CXX=mpicxx',
                                   './get_deps.sh',
                                   './build_all.sh'],
                            repository='https://gitlab.com/ompcluster/task-bench.git')

    hpccm.config.set_container_format('docker')

    return Stage0


def combine(list1, list2):
    return map(lambda x: '-'.join(x), list(itertools.product(list1, list2)))


def main():
    print("Generating Runtime image definitions...")

    # list of building blocks combinations to use
    allConf = ["ubuntu18.04", "ubuntu20.04", "centos7"]
    allConf += combine(allConf, ["cuda9.2", "cuda10.1", "cuda10.2", "cuda11.2"])
    allConf = combine(allConf, ["mpich", "openmpi", "mvapich2"])

    # print(*allConf)
    outputFolder = "Dockerfiles"

    for currConf in allConf:
        baseImage = "ompcluster/hpcbase:" + currConf

        defFileTextStarPU = str(generateRecipeStarPU(baseImage))
        defFileNameStarPU = "taskbench-starpu-" + currConf

        defFileTextCharmMPI = str(generateRecipeCharmMPI(baseImage))
        defFileNameCharmMPI = "taskbench-charm-mpi-" + currConf

        defFileTextCharmUCX = str(generateRecipeCharmUCX(baseImage))
        defFileNameCharmUCX = "taskbench-charm-ucx-" + currConf

        defFileTextChapel = str(generateRecipeChapel(baseImage))
        defFileNameChapel = "taskbench-chapel-" + currConf

        defFileTextLegion = str(generateRecipeLegion(baseImage))
        defFileNameLegion = "taskbench-legion-" + currConf

        #defFileTextRegent = str(generateRecipeRegent(baseImage))
        #defFileNameRegent = "taskbench-regent-" + currConf

        # save container definition file
        with open(f"{outputFolder}/{defFileNameStarPU}", "w") as text_file:
                text_file.write(str(defFileTextStarPU))

        with open(f"{outputFolder}/{defFileNameCharmMPI}", "w") as text_file:
                text_file.write(str(defFileTextCharmMPI))

        with open(f"{outputFolder}/{defFileNameCharmUCX}", "w") as text_file:
                text_file.write(str(defFileTextCharmUCX))

        with open(f"{outputFolder}/{defFileNameChapel}", "w") as text_file:
                text_file.write(str(defFileTextChapel))

        with open(f"{outputFolder}/{defFileNameLegion}", "w") as text_file:
                text_file.write(str(defFileTextLegion))

        #with open(f"{outputFolder}/{defFileNameRegent}", "w") as text_file:
        #        text_file.write(str(defFileTextRegent))


if __name__ == "__main__":
    main()
